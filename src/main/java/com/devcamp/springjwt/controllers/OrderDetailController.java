package com.devcamp.springjwt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.springjwt.models.OrderDetails;
import com.devcamp.springjwt.security.services.OrderDetailService;


@RestController
@CrossOrigin
@RequestMapping("/fastfood/api/noauth/")
public class OrderDetailController {
    @Autowired
    OrderDetailService orderDetailService;

    @PostMapping("orderDetail/{orderId}/{productId}")
    public ResponseEntity<Object> createOrderDetails(@PathVariable Integer orderId, @PathVariable Integer productId, @RequestBody OrderDetails orderDetails) {
        return orderDetailService.createOrderDetails(orderId, productId, orderDetails);
    }
    
    @GetMapping("orderDetail")
    public ResponseEntity<Object> getAllOrderDetail() {
        return orderDetailService.getAllOrderDetail();
    }
}

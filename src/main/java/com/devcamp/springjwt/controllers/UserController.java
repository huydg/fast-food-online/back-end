package com.devcamp.springjwt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.springjwt.models.User;
import com.devcamp.springjwt.security.services.UserService;

@RestController
@CrossOrigin
@RequestMapping("/fastfood/api/noauth/")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("user/{username}")
    public ResponseEntity<User> getUserByUsername(@PathVariable String username) {
        return userService.getUserByUsername(username);
    }

    @PutMapping("user/{username}")
    public ResponseEntity<User> updateUser(@PathVariable String username, @RequestBody User user) {
        return userService.updateUser(username, user);
    }

    @PutMapping("user/{username}/payment")
    public ResponseEntity<User> updatePayment(@PathVariable String username, @RequestBody User user) {
        return userService.updateUserPayment(username, user);
    }
}

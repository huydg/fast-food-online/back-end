package com.devcamp.springjwt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.springjwt.models.Order;
import com.devcamp.springjwt.security.services.OrderService;

@RestController
@CrossOrigin
@RequestMapping("/fastfood/api/noauth/")
public class OrderController {
    @Autowired
    OrderService orderService;

    @PostMapping("/order/{id}")
    public ResponseEntity<Object> createOrder(@PathVariable Long id, @RequestBody Order order) {
        return orderService.createOrder(id, order);
    }   

    @GetMapping("order/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable Integer id) {
        return orderService.getOrderById(id);
    }

    @GetMapping("order")
    public ResponseEntity<Object> getAllOrder() {
        return orderService.getAllOrder();
    }

    @DeleteMapping("order/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable Integer id) {
        return orderService.deleteOrder(id);
    }
}

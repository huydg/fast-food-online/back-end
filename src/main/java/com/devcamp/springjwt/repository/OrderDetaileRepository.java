package com.devcamp.springjwt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.springjwt.models.Order;
import com.devcamp.springjwt.models.OrderDetails;

public interface OrderDetaileRepository extends JpaRepository<OrderDetails, Integer> {
    
}

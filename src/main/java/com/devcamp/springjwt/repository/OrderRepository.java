package com.devcamp.springjwt.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.springjwt.models.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {
   
}

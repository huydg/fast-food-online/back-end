package com.devcamp.springjwt.security.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.springjwt.models.User;
import com.devcamp.springjwt.repository.UserRepository;
@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public ResponseEntity<User> getUserByUsername(String username) {
        try {
            Optional<User> userOptional = userRepository.findByUsername(username);  
            if(userOptional.isPresent()) {
                User _user = userOptional.get();
                return new ResponseEntity<User>(_user, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // cập nhật thông tin người dùng
    public ResponseEntity<User> updateUser(String username, User user) {
        try {
            Optional<User> userOptional = userRepository.findByUsername(username);
            if(userOptional.isPresent()) {
                User _user = userOptional.get();
                _user.setFirstName(user.getFirstName());
                _user.setLastName(user.getLastName());
                _user.setEmail(user.getEmail());
                _user.setPhoneNumber(user.getPhoneNumber());
                _user.setAddress(user.getAddress());
                _user.setCity(user.getCity());
                _user.setState(user.getState());
                return new ResponseEntity<User>(userRepository.save(_user), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // cập nhật thông tin thanh toán
    public ResponseEntity<User> updateUserPayment(String username, User user) {
        try {
            Optional<User> userOptional = userRepository.findByUsername(username);
            if(userOptional.isPresent()) {
                User _user = userOptional.get();
                _user.setNameBank(user.getNameBank());
                _user.setNameCard(user.getNameCard());
                _user.setNumberCard(user.getNumberCard());
                _user.setExpiryDate(user.getExpiryDate());
                return new ResponseEntity<User>(userRepository.save(_user), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

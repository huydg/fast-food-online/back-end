package com.devcamp.springjwt.security.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.springjwt.models.Order;
import com.devcamp.springjwt.models.User;
import com.devcamp.springjwt.repository.OrderRepository;
import com.devcamp.springjwt.repository.UserRepository;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    UserRepository userRepository;

    public ResponseEntity<Object> createOrder(Long id, Order order) {
        try {
            Optional<User> useOptional = userRepository.findById(id);
            if(useOptional.isPresent()) {
                Order _newOrder = new Order();
                _newOrder.setUser(useOptional.get());
                _newOrder.setOrderDate(new Date());
                _newOrder.setTotalPayment(order.getTotalPayment());
                return new ResponseEntity<Object>(orderRepository.save(_newOrder), HttpStatus.CREATED);
            }
            else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    public ResponseEntity<Object> getOrderById(Integer id) {
        try {
            Optional<Order> orderOptional = orderRepository.findById(id);
            if(orderOptional.isPresent()) {
                return new ResponseEntity<Object>(orderOptional.get(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<Object>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> getAllOrder() {
        return new ResponseEntity<Object>(orderRepository.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<Object> deleteOrder(Integer id) {
        orderRepository.deleteById(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}

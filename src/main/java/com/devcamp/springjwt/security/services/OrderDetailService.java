package com.devcamp.springjwt.security.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.springjwt.models.Order;
import com.devcamp.springjwt.models.OrderDetails;
import com.devcamp.springjwt.models.Product;
import com.devcamp.springjwt.repository.OrderDetaileRepository;
import com.devcamp.springjwt.repository.OrderRepository;
import com.devcamp.springjwt.repository.ProductRepository;

@Service
public class OrderDetailService {
    @Autowired
    OrderDetaileRepository orderDetaileRepository;

    @Autowired 
    OrderRepository orderRepository;

    @Autowired
    ProductRepository productRepository;

    // tạo mới orderdetail
    public ResponseEntity<Object> createOrderDetails(Integer orderId, Integer productId ,OrderDetails orderDetail) {
      try {
        Optional<Order> ordeOptional = orderRepository.findById(orderId);
        Optional<Product> producOptional = productRepository.findById(productId);
            OrderDetails newOrderDetails = new OrderDetails();
            newOrderDetails.setOrder(ordeOptional.get());
            newOrderDetails.setProduct(producOptional.get());
            newOrderDetails.setQuantity(orderDetail.getQuantity());
            return new ResponseEntity<Object>(orderDetaileRepository.save(newOrderDetails),HttpStatus.CREATED);
      } catch (Exception e) {
        // TODO: handle exception
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }

    // get order detail
    public ResponseEntity<Object> getAllOrderDetail() {
        return new ResponseEntity<Object>(orderDetaileRepository.findAll(), HttpStatus.OK);
    }

    
    
}
